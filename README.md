# PDepend

PDepend is a language-agnostic dependency management system writtin in Python.

Use this tool when you want to share depdendencies among multiple projects when the project's programming language / environment don't make this otherwise possible or easy.  

## Requirements
  - [Git](https://git-scm.com/downloads)
  - [Python 3.6+](https://www.python.org)
  - The commands "git" and "python" must be available from the command line.
  - If your project requires compilation, you must have your environment setup so that you can do this from the command line.

## Installation
Place the pdepend.py script in the project's root directory.  Use the following command to create a "skeleton" setup for you:
```sh
python pdepend.py --new
```
This will create two files that you must modify.

**pdepend.config.json** : This configuration file controls how pdepend works.  It contains the following settings:
  - build_command: This command is used for projects that require compilation
  - project_title: This is the title of your project.
  - clean_ignore_files: pdepend will check to make sure your project is "clean" (does not have local uncommitted modifications) before it will complete the build command.  This setting is a list of files you wish to ignore from this check.
  - project_dependency_relative_dir: You can leave this setting with the default value, unless you have some reason to change it.  This is the folder under which pdepend will place all the project dependency code.  Currently pdepend expects this to be a folder in the project root of the project's directory, so just use a simple folder name, and pdepend will sort out the full path name based on the project's root directory.

**pdepend.dependencies.json** : This is where you define all of your dependencies.  The example provided with the "new" command from the Installation should show you what is needed.

## Usage

To see all the available commmands, type:
```sh
python pdepend.py --help
```

## TODO

We need to have a better / more secure way of handle git commands from the command line.  Currently we prompt the user for a git username and password, and then we use that in the git pull requests.  This is not only insecure from the request standpoint, but also because these credentials end up getting stored in the .git/config file for each repository.  Pdepend deletes the credentials from these files after each pull is complete, but this is still not an ideal situation.  I'd prefer to have "git pull" have the typical command-line functionality, but I haven't yet gotten that to work with the subprocess command. 

## License

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to http://unlicense.org/
