#!/usr/bin/python
"""
    File name: PDepend.py
    Description: Language-agnostic dependency management script.  pdepend.py --help for more info.
    Date created: 5/1/2016
    Version: 2.2

    This is free and unencumbered software released into the public domain.
    http://unlicense.org
"""

import sys
import json
import os
import subprocess
import getpass

VERSION = "2.1"

PROJECT_ROOT_DIR = ""
PROJECT_DEPENDENCY_ABSOLUTE_DIR = ""
DEPENDENCIES_JSON_FILENAME = "pdepend.dependencies.json"
CONFIG_JSON_FILENAME = "pdepend.config.json"
APPCFG = {}
DEPENDENCIES = {}


#---------------------------------------------------------------------------------------
# HELP functions
#---------------------------------------------------------------------------------------

def print_instructions():
  """
  Help information
  """
  str = "NAME\r\n"
  str += "    PDepend - Language-Agnostic Dependency Management Script\r\n\r\n"

  str += "SYNOPSIS\r\n"
  str += "    python pdepend.py [--sync] [--build] [--new]\r\n\r\n"

  str += "DESCRIPTION\r\n"
  str += "    This is a script written in Python to help applications manage\r\n"
  str += "    their dependencies when they don't have a good way of doing  \r\n"
  str += "    this on their own.\r\n\r\n"

  str += "    SYNC: Synchronizes every dependency listed in the \r\n"
  str += "    pdepend.dependencies.json file.  Pdepend currently only supports\r\n"
  str += "    Git repositories as depdenencies, and each dependency must list\r\n"
  str += "    the Git repository name, url, and tag to be used.\r\n\r\n"

  str += "    BUILD: Allows you to compile / build the project.  This build\r\n"
  str += "    command must be specified in pdepend.config.json and will be\r\n"
  str += "    specific to your environment and programming language.  Why\r\n"
  str += "    use PDepend to build your project?  Because it first checks\r\n"
  str += "    to make sure that all your dependencies are correct.  It goes\r\n"
  str += "    through pdepend.dependencies.json and makes sure every \r\n"
  str += "    dependency is checked out locally, at the right tag, and doesn't\r\n"
  str += "    contain any local uncommitted modifications.  Only then will it\r\n"
  str += "    allow the build to continue.  This really helps you guarantee\r\n"
  str += "    later on what the state of each dependency was in at the time\r\n"
  str += "    the project was built.\r\n\r\n"
 
  str += "    NEW: If you're just starting out with PDepend, this is a helpful\r\n"
  str += "    command.  It will create skeleton versions of the config files\r\n"
  str += "    needed, to demonstrate what options are available.\r\n\r\n"

  str += "OPTIONS\r\n"
  str += "    --sync   Synchronize dependencies listed in the\r\n\r\n"
  str += "    --build  Build / compile your project\r\n\r\n"
  str += "    --new    Creates default config files\r\n\r\n"
  
  print (str)
  


#---------------------------------------------------------------------------------------
# GENERAL
# Functions used by various modes
#---------------------------------------------------------------------------------------

def print_title():
  """
  Prints the title
  """
  str = "\r\n"
  str += "***************************************************************************\r\n"
  str += "* PDepend\r\n"
  str += "*\r\n"
  str += "* Language-Agnostic Dependency Management\r\n"
  str += "*\r\n"
  str += "* Version: " + VERSION + "\r\n"
  str += "*\r\n\r\n"
  str += "* This is free and unencumbered software released into the public domain.\r\n"
  str += "* http://unlicense.org\r\n"
  str += "***************************************************************************\r\n\r\n"
  print (str)

def print_mode(mode):
  """
  Prints the mode (--new, --sync, --build)
  """
  print ("Mode: " + mode + "\r\n----------------------\r\n\r\n")
  
  
def exit_now():
  """
  Does any necessary cleanup for exiting
  """
  cd_project_root()
  sys.exit()
  
def git(*args):
  """
  Supports "git" commands.
  """
  return subprocess.check_call(['git'] + list(args))
  
def save_config(filename, config):
  """
  Saves the config object to a json file
  """
  config_formatted = json.dumps(config, indent=4)
  with open(filename, 'w') as fp:
    print >> fp, config_formatted

def get_json_data(filename):
  """
  Loads json data from file, converts to dictionary object
  """
  with open(filename) as json_data:
    json_dict = json.load(json_data)
    json_data.close()
  return json_dict

def cd_project_root():
  """
  Changes to the project's root directory
  """
  if PROJECT_ROOT_DIR == "":
    print ("ERROR: PROJECT_ROOT_DIR not defined!")
    sys.exit()
  os.chdir(PROJECT_ROOT_DIR)
  
def cd_dependency_root():
  """
  Changes to the root directory inside of which all project
  dependencies reside.
  """
  os.chdir(PROJECT_DEPENDENCY_ABSOLUTE_DIR)

def make_new_url(url, username, password):
  """
  Inserts the username and password just entered by the user into the url for authentication
  """
  if url.startswith("ssh"):
    str = "ERROR: url is invalid: " + url + ".  \r\nPDepend only works with HTTP/s urls, "
    str += "becase HTTP supports providing the \r\n"
    str += "username and password in the request.  Ex:\r\n\r\n"
    str += "http://username.password@url/to/git/repo.git\r\n\r\n"
    str += "You will need to modify this url value manually in " + DEPENDENCIES_JSON_FILENAME + "\r\n"
    str += "To find the HTTP url for this repository, go into the repo in Git Stash, then click \"Clone\"\r\n"
    str += "If you see \"SSH\" selected, select \"HTTP\" instead, then copy the url and paste into the configuration file.\r\n\r\n"
    print (str)
    exit_now()

  #Replace beginning of url with username and password the user entered    
  parts = url.split("@")
  return "http://" + username + ":" + password + "@" + parts[1]



#---------------------------------------------------------------------------------------
# GIT REPO FUNCTIONS
# Functions for handling git repositories
#---------------------------------------------------------------------------------------

def check_project_repo_clean():
  cd_project_root()
  output = subprocess.check_output(["git", "status"])

  # Clean if nothing to commit
  if "nothing to commit" in output.decode():
    return True

  # Unclean if something to commit with no specified files to ignore
  if len(APPCFG["clean_ignore_files"]) == 0:
    print_build_project_repo_unclean()
    exit_now()
    
  # Checking modified / untracked files to see if any are specified as files to ignore
  # Just check every line. If it's not a file, ignore it.  Could be a statement,
  # like: (use "git add <ile>..." to include blah blah), which we don't care about.
  for line in output.decode().split("\n"):
    afile = line.strip()
    if os.path.isfile(afile):
      if afile not in APPCFG["clean_ignore_files"]:
        print_build_project_repo_unclean()
        exit_now()


def ensure_gitignore():
  cd_project_root()

  already_exists = False

  with open(".gitignore","a+") as f:
    f.seek(0)
    for line in f:
      if line.strip() == "/" + APPCFG['project_dependency_relative_dir']:
        already_exists = True
        break

    if not already_exists:
      f.write("/" + APPCFG['project_dependency_relative_dir'] + "\n")


def repo_exists(name):
  exists = False

  cd_dependency_root()

  if os.path.isdir(name):
    exists = True

  cd_project_root()
  return exists


def repo_missing_git_dir(name):
  missing = False
  cd_dependency_root()
  
  if not os.path.isdir(name + "/.git"):
    missing = True

  cd_project_root()
  return missing


def repo_is_clean(name):

  clean = False
  
  cd_dependency_root()
  os.chdir(name)
  output = subprocess.check_output(["git", "status"])

  if "nothing to commit" in output.decode():
    clean = True

  cd_project_root()
  return clean


def repo_is_correctly_tagged(name, git_tag):

  cd_dependency_root()
  os.chdir(name)
  output = subprocess.check_output(["git", "describe", "--tag"])  

  if not git_tag in output.decode():
    return False
  return True


def check_tags_match(name, git_tag):
  cd_dependency_root()
  os.chdir(name)

  #get local repo tag, compare with tag defined in config file
  output = subprocess.check_output(["git", "describe", "--tag"])
  output = output.strip()
  
  if not git_tag == output.decode():
    print ("Error: " + name + ": Expected git tag " + git_tag + "; found " + output.decode())
    exit_now()
      
  cd_project_root()


def clone_new_repo(name, url, git_tag):
  cd_dependency_root()
  git("clone", url, name)
  os.chdir(name)
  git("checkout", git_tag)
  cd_project_root()


def sync_repo(name, url, git_tag):

  # pull and checkout the correct tag
  cd_dependency_root()
  os.chdir(name)
  git("pull", url, "--tags")
  #git("pull", "origin", "--tags")
  git("checkout", git_tag)

  cd_project_root()



#---------------------------------------------------------------------------------------
# SETUP
# General start up instructions no matter what mode is chosen
#---------------------------------------------------------------------------------------

def setup():

  global PROJECT_ROOT_DIR
  PROJECT_ROOT_DIR = os.path.dirname(os.path.abspath(__file__))  
  
  global APPCFG
  APPCFG = get_json_data(CONFIG_JSON_FILENAME)

  global DEPENDENCIES
  DEPENDENCIES = get_json_data(DEPENDENCIES_JSON_FILENAME)

  global PROJECT_DEPENDENCY_ABSOLUTE_DIR
  proj_dep_rel = APPCFG['project_dependency_relative_dir']
  if proj_dep_rel == "":
    proj_dep_rel = "."
  PROJECT_DEPENDENCY_ABSOLUTE_DIR = os.path.join(PROJECT_ROOT_DIR, proj_dep_rel)

  if not os.path.isdir(PROJECT_DEPENDENCY_ABSOLUTE_DIR):
    os.mkdir(PROJECT_DEPENDENCY_ABSOLUTE_DIR)

  ensure_gitignore()



#---------------------------------------------------------------------------------------
# NEW mode
#---------------------------------------------------------------------------------------

def create_new():
  """
  Creates default configurations to be modified per project as needed
  """
  
  # pdepend.config.json
  global APPCFG
  APPCFG = {
    "project_title": "ProjectName",
    "project_dependency_relative_dir": "project_dependencies",
    "clean_ignore_files": [
      ".gitignore",
      "pdepend.config.json",
      "pdepend.dependencies.json",
    ],
    "build_command": "vb6.exe /Make ProjectName"
  }
  save_config(CONFIG_JSON_FILENAME, APPCFG)

  # pdepend.dependencies.json
  global DEPENDENCIES
  DEPENDENCIES = {
    "RepoName": {
      "url": "http://username.password@url/to/git/repo.git",
      "git_tag": "1.0"
    }
  }
  save_config(DEPENDENCIES_JSON_FILENAME, DEPENDENCIES)



#---------------------------------------------------------------------------------------
# SYNC mode
#---------------------------------------------------------------------------------------
  
def sync_dependencies():

  setup()

  username = input("Please enter your username: ")
  password = getpass.getpass("Please enter your password: ")
  print ("\r\n")
  
  for d_name, d_settings in DEPENDENCIES.items():
    sync_dependency(d_name,
                    make_new_url(d_settings['url'], username, password),
                    d_settings['git_tag'])

  for d_name, d_settings in DEPENDENCIES.items():
    secure_dependency_gitconfig_url(d_name, username, password)

def sync_dependency(name, url, git_tag):
  """
  If a directory doesn't exist called 'name', mkdir and clone
  If a directory already exists, do git status.
    If there are local changes, error and bail.  Otherwise, sync.
  """
  
  # ensure repo exists in the first place
  if not repo_exists(name):
    clone_new_repo(name, url, git_tag)
    return

  # if someone accidentally removed the ".git" folder, bail
  if repo_missing_git_dir(name):
    print ("Error: The " + name + " directory exists, but it is not a git repository.  I'm outta here!!!")
    exit_now()
    
  # if there are uncommitted changes, bail
  if not repo_is_clean(name):
    print ("Error: The " + name + " repository has local, uncommitted changed.  I'm outta here!!!")
    exit_now()
    
  # if here, all is well.  sync.
  sync_repo(name, url, git_tag)

def secure_dependency_gitconfig_url(name, username, password):
  """
  This removes the password from .git/config for each dependency repo, so manual git pull will
  require the user to enter their password manually and it won't be stored in a plain text file!
  """
  cd_dependency_root()
  os.chdir(name + "\.git")

  with open('config', 'r') as f:
    newlines = []
    for line in f.readlines():
      newlines.append(line.replace(username + ":" + password + "@", username + "@"))
                      
  with open('config', 'w') as f:
    for line in newlines:
      f.write(line)

  cd_project_root()

#---------------------------------------------------------------------------------------
# BUILD mode
#---------------------------------------------------------------------------------------

def build():

  setup()
  check_build_dependencies()
  check_project_repo_clean()
  build_project()


def check_build_dependencies():
  """
  Check all dependencies are in tact before building.
  """
  for d_name, d_settings in DEPENDENCIES.items():
    check_build_dependency(d_name, d_settings['git_tag'])


def check_build_dependency(name, git_tag):
  """
  Check a dependency to determine whether we should allow building
  """
  
  # Does repo exit?
  if not repo_exists(name):
    print ("Error: The " + name + " respository was not found.  Run this command again with --sync.  I'm outta here!!!\r\n")
    exit_now()
    return

  # Is the ".git" folder missing?
  if repo_missing_git_dir(name):
    print ("Error: The " + name + " directory exists, but it is not a git repository.  I'm outta here!!!\r\n")
    exit_now()
    
  # Are there uncommitted changes?
  if not repo_is_clean(name):
    print ("Error: The " + name + " repository has local, uncommitted changed.  I'm outta here!!!\r\n")
    exit_now()

  # Is the wrong tag checked out?
  if not repo_is_correctly_tagged(name, git_tag):
    strerr  = "Error: The " + name + " repository is defined in " + DEPENDENCIES_JSON_FILENAME + " with the git tag " + git_tag
    strerr += ".  However, this doesn't match the repository on this local machine.  Either update the config file type "
    strerr += "\"python.py pdepend.py --sync\" to get your local dependencies back in sync with your config file."
    print (strerr)
    exit_now()
    
  check_tags_match(name, git_tag)


def build_project():
  subprocess.call(APPCFG["build_command"], shell=True)


def print_build_project_repo_unclean():
  str = "Error: " + APPCFG["project_title"] + ": This project has local, uncommitted changes.  We only build projects\r\n"
  str += "that have gone through the whole process from development and testing, up until the code\r\n"
  str += "has been merged into master and tagged.  I'm outta here!!!\r\n"
  print (str)



#---------------------------------------------------------------------------------------
# MAIN entry point into script
#---------------------------------------------------------------------------------------

def main(argv):

    print_title()
    
    for arg in sys.argv:

        if arg in ('-h', '--help', '/?'):
            print_instructions();
            sys.exit()
            
        if arg == '--new':
            print_mode("New")
            create_new()
            sys.exit()
            
        if arg == '--sync':
            print_mode("Sync")
            sync_dependencies()
            sys.exit()
            
        if arg == '--build':
            print_mode("Build")
            build()
            sys.exit()

    print_instructions()
    
if __name__ == "__main__":
   main(sys.argv[1:])
